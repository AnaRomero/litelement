import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {
    
    static get properties() {
        //MODELO
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    //método constructor de la clase. Se ejecuta cuando se invoca a la clase.
    constructor(){
        super();

        this.name = "Prueba de nombre";
        this.yearsInCompany = 12;
        this.photo = {
            src: "./img/persona.jpg",
            alt: "foto persona"
        };

        this.updatePersonInfo();
    }

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) => {
            console.log("[updated] Propiedad " + propName + " cambia valor, anterior era " + oldValue );
        });
        if(changedProperties.has("name")){
            console.log("[updated] Propiedad name cambia valor anterior era "
            + changedProperties.get("name") 
            + " nuevo es " 
            + this.name);
        };

        if(changedProperties.has("yearsInCompany")){
            this.updatePersonInfo();
        };
        
    }

    render(){
        //VISTA - PLANTILLA
        return html`
            <div>
                <label>Nombre Completo</label>
                <!--Suscribimos al evento updateName que vamos a crear para el evento que puede ser change,input...-->
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
                <img src="${this.photo.src}"  height="" width="" alt="${this.photo.alt}"></img>
            </div>
        `;
    }

    // Creamos función manejadora del evento
    updateName(e){
        console.log("[updateName] Actualización propiedad nombre con el valor: " + e.target.value); 
        this.name = e.target.value;

    }

    updateYearsInCompany(e){
        console.log("[updateYearsInCompany] Actualización propiedad yearsInCompany con el valor: " + e.target.value);
        this.yearsInCompany = e.target.value;
        
        /*
        if (e.target.value >= 7){
            this.personInfo = "lead";
        }else if(e.target.value >= 5){
            this.personInfo = "senior";
        }else if(e.target.value >= 3){
            this.personInfo = "team";
        }else {
            this.personInfo = "junior"
        }
        */

    }

    updatePersonInfo(){
        
        if (this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personInfo = "senior";
        }else if(this.yearsInCompany >= 3){
            this.personInfo = "team";
        }else {
            this.personInfo = "junior"
        }
    }
    
}

customElements.define('ficha-persona', FichaPersona);