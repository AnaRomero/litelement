import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

    static get properties(){
        return{
            totalPeople:{type: Number},

            //CODIGO DE CLASE
            peopleStats:{type: Object}
            
        }       
    }

    //CODIGO DE CLASE
    
    constructor(){
        super();
        this.peopleStats = {};

    }
    
    render(){
        return html`
        <!--importamos bootstrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <aside>
                <section>
            
                  <div>
                    Hay <span class ="badge badge-pill badge-primary">${this.peopleStats.numberofPeople}</span> personas
                   <div class="mt-1">
                        <input type="range" min="0" max="${this.peopleStats.maxYearsInCompany}" step = "1" 
                        @input="${this.updateMaxYearsInCompanyFilter}" 
                        value = "${this.peopleStats.maxYearsInCompany}"/>
                   </div> 
                    <div>
                        <button class ="w-100 btn btn-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                </section>
            </aside>
        `;
    }

    newPerson(e){
        console.log("[newPerson] En persona-sidebar: Se va a crear una persona nueva");

       
        //No hay que enviar nada.. es suficiente con saber que el botón se ha pulsado
        this.dispatchEvent(new CustomEvent("new-person", {}));
    }


    updateMaxYearsInCompanyFilter(e){
        console.log("[updateMaxYearsInCompanyFilter] Se ha filtrado por Años en la empresa = " + e.target.value);

        this.dispatchEvent(
            new CustomEvent("updated-people-stats", {
                detail:{
                    maxYearsInCompany: e.target.value
                }
        }));
    }
}

customElements.define('persona-sidebar', PersonaSidebar);