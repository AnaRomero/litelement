import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';


class PersonaApp extends LitElement {

    static get properties(){
        return{
            people: {type: Array}
        }
    }

    updated(changedProperties){
        if(changedProperties.has("people")){
            console.log("updated en persona-app ");
            console.log(this.people);
            this.shadowRoot.querySelector("persona-stats").people = this.people;
            
            
        }
    }
    //PLANTILLA
    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class = "col-2" @new-person="${this.newPerson}" 
                @updated-people-stats = "${this.newMaxYearsInCompanyFilter}"></persona-sidebar>
                <persona-main class = "col-10" @updated-people="${this.updatePeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <!-- Creamos un componente para la realización de calculos, pero no se utilizará para pintar nada-->
            <persona-stats @updated-people-stats ="${this.updatePeopleStats}"></persona-stats>
            
            
        `;
    }


    newPerson(e){
        console.log("[newPerson] En persona-app");
        // con el método updated en persona-main, la línea siguiente fuerza que salte el updated, porque cambia el valor del atributo showPersonForm de persona-mail
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatePeople(e){
        console.log("[updatedPeople] En persona-app");
        console.log(e.detail.people);
        //Envio el listado de personas a stast para su tratamiento
        this.people = e.detail.people;
        
    }

    updatePeopleStats(e){
        console.log("[updatePeopleStats] Se va a actualizar el total de personas en persona-app con el valor " + e.detail.peopleStats.numberofPeople);
        console.log("[updatePeopleStats] Se va a actualizar el maxYearsInCompany " + e.detail.peopleStats.maxYearsInCompany);
        //this.people = e.detail.people;
        this.shadowRoot.querySelector("persona-sidebar").peopleStats =e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter =e.detail.peopleStats.maxYearsInCompany;
    }

    newMaxYearsInCompanyFilter(e){
        console.log("newMaxYearsInCompanyFilter Nuevo Filtro es " + e.detail.maxYearsInCompanyFilter);

        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany; 
    }




}

customElements.define('persona-app', PersonaApp);