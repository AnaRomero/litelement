import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement {
    
    static get properties(){
        return{
            people: {type: Array}
        }
    }    

    constructor(){
        super();

        this.people = [
            {
                name: "Ana Romero",
                yearsInCompany: "10",
                profile:"Lorem ipsum dolor sit amet.",
                photo:{
                    src: "./img/persona.jpg",
                    alt: "Ana Romero"
                }
            },{
                name: "Pepe Sánchez",
                yearsInCompany: "2",
                profile:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam iriure dolor in hendrerit in vulputate velit esse molestie consequat.",
                photo:{
                    src: "./img/persona.jpg",
                    alt:"Pepe Sánchez"
                }
            },{
                name:"Lola López",
                yearsInCompany: "5",
                profile:"Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                photo:{
                    src: "./img/persona.jpg",
                    alt:"Lola López"
                }
            },{
                name: "Nombre de persona",
                yearsInCompany: "9",
                profile:"Lorem ipsum dolor sit amet.",
                photo:{
                    src: "./img/persona.jpg",
                    alt:"Lola López"
                }

            },{
                name: "Otra persona",
                yearsInCompany: "11",
                profile:"Lorem ipsum dolor sit amet.",
                photo:{
                    src: "./img/persona.jpg",
                    alt:"Lola López"
                }
            }
        ];
    }

    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people");

            this.dispatchEvent(
                new CustomEvent("people-data-updated",
                {
                    detail:{
                        people: this.people
                    }
                }

                )
            )
        }
    }
}

customElements.define('persona-main-dm', PersonaMainDM);