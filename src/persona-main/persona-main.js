import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {
    
    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean},
            maxYearsInCompanyFilter: {type: Number}
            
        };
    }

    /*static get styles(){
        return css`
            :host {
                all: initial;
            }
        `;
    }*/

    constructor(){
        super();

        this.people = [];

        this.showPersonForm = false;
        this.maxYearsInCompanyFilter = 0;
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>

            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => parseInt(person.yearsInCompany) <= this.maxYearsInCompanyFilter
                    ).map(
                        person => html`<persona-ficha-listado 
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                                >
                            </persona-ficha-listado>`
                    )}    
                </div>

            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none" @persona-form-close="${this.personFormClose}" @persona-form-store="${this.personFormStore}"></persona-form>
            </div>
            <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
            
        `;
    }   

    peopleDataUpdated(e){
        this.people = e.detail.people;
    }

    //En este caso, utilizamos esta función para modificar propiedades en base a un evento. No podemos utilizar updated para pintar o escribir nada en formularios porque no pinta nada.
    updated(changeProperties){
       // console.log("updated");

        if(changeProperties.has("showPersonForm")){
          //  console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }

        }
        if(changeProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
           
            this.dispatchEvent(new CustomEvent("updated-people",{
                detail:{
                    people: this.people,
                    
                }
            }
            ));
        }

        if(changeProperties.has("maxYearsInCompanyFilter")){
            console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter en persona-main");
            console.log("Se van a mostrar las personas cuya antiguedad máx sera "+ this.maxYearsInCompanyFilter);
           
            
        }
    }

    personFormClose(){
        console.log("[personFormClose] Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("[personFormStore] Se va a almacenar una persona");
        
        console.log(e.detail);

        if (e.detail.editingPerson === true){
            // Se está editando un registro ya existente
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);
            //Buscamos en el array la persona a actualizar

           /* let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            ) ;
                //Si no se encuentra el índice, devuelve -1
            if(indexOfPerson >= 0){
                console.log("Persona encontrada en indice" + indexOfPerson);
                this.people[indexOfPerson]= e.detail.person;
            }*/
          
            //haciendo la edición del dato así, forzamos  que se pase por updated.
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
            )
        }else{
            //se está creando un registro nuevo
            //Actualizamos añadiendo el registro adicional. Esta opción push no actualiza el array entero y provoca que no se pase por el updated()
            //this.people.push(e.detail.person);

            console.log ("Se va a almacenar una persona nueva");
            // Con los tres puntos hacemos una numeración de los elementos del array, evaluando uno tras otro y no el array completo
            this.people = [...this.people, e.detail.person];
            
        }
        
        console.log("[personFormStore] Persona almacenada");
        this.showPersonForm = false;
    }

    showPersonFormData(){
        console.log("[showPersonFormData] Mostrando el formulario de persona");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }

    showPersonList(){
        console.log("[showPersonList] Ocultando el formulario de persona");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }


    deletePerson(e){
        console.log("[deletePerson] deletePerson en persona-main");
        console.log("[deletePerson] Se va a borrar la persona de nombre: " + e.detail.name);
        //Se actualiza person y se mantiene sólo si el nombre es diferente del que llega en el evento. Esta opción genera un nuevo array ocn el filter indicado y hace que se pase por updated
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    //Función manejadora
    infoPerson(e){
        console.log("[infoPerson] Se ha pedido más información de la persona de nombre " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )

        console.log(chosenPerson);
        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

    }
}

customElements.define('persona-main', PersonaMain);