import { LitElement, html } from 'lit-element';


class PersonaStats extends LitElement {

    static get properties(){
        return{
            people: {type: Array},
            maxYearsInCompany: {type: Number}
        };
    }

    constructor(){
        super();

        this.people = [];
        this.maxYearsInCompany = 100;
    }

    updated(changeProperties){
        console.log("Se ha actualizado properties de persona-stats");

        //CODIGO DE CLASE
        if(changeProperties.has("people")){
            console.log("[updated] Se ha actualizado people en persona-stats ");
            console.log(this.people);
            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail:{
                    peopleStats: peopleStats
                }
            }))
        }
    }

    //CODIGO DE CLASE
    gatherPeopleArrayInfo(people){
        let peopleStats = {};

        peopleStats.numberofPeople = people.length;
        console.log("longitud de people es " + peopleStats.numberofPeople);
        let maxYearsInCompany= 0;
        console.log(people);
        people.forEach( person => {
            if(parseInt(person.yearsInCompany) > maxYearsInCompany){
                maxYearsInCompany = person.yearsInCompany;
            }
    });
        console.log("maxYearsInCompany es "+ maxYearsInCompany)
        peopleStats.maxYearsInCompany = maxYearsInCompany;


        return peopleStats;
    }




}

customElements.define('persona-stats', PersonaStats);