import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties(){
        return{
            person:{type: Object},
            editingPerson: {type: Boolean}
        }       
    }

    constructor(){
        super();

        this.resetFormData();
    }
    
    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre Completo</label>
                    <!-- con el .value hacemos databinding para enlazar el dato desde persona-main cuando se pulse en Info -->
                    <input type="text" id="personFormName" class="form-control" placeholder="Nombre Completo" 
                    @input="${this.updateName}" 
                    .value = "${this.person.name}"
                    ?disabled="${this.editingPerson}"/>
                    <!-- el punto anterior de disabled con el interrogante es para hacer una lógica con un boolean , que es editingPerson -->
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea class="form-control" placeholder="Perfil" rows="5" @input="${this.updateProfile}" .value = "${this.person.profile}"></textarea>
                 </div>
                 <div class="form-group">
                    <label>Años en la empresa</label>
                    <input type="text" class="form-control" placeholder="Años en la empresa" @input ="~${this.updateYearsInCompany}" .value = "${this.person.yearsInCompany}"/>
                </div>
                <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
                <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
            </form>
        </div>
        `;
    }

    goBack(e){
        
        e.preventDefault();
        console.log("[goBack]"); 
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        
        this.resetFormData();
    }
    

    storePerson(e){
        
        e.preventDefault();
        console.log("[storePerson] La propiedad name vale " + this.person.name);
        console.log("[storePerson] La propiedad profile vale " + this.person.profile);
        console.log("[storePerson] La propiedad yearsInCompany vale " + this.person.yearsInCompany);

        this.person.photo ={
            src: "./img/persona.jpg",
            alt:"Persona"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail:{
                person:{
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        })
        );

    }

    updateName(e){
        console.log("[updateName] Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
        
    }

    updateProfile(e){
        console.log("[updateProfile] Actualizando la propiedad profile con el valor " + e.target.value);
         this.person.profile = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("[updateYearsInCompany] Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;

    }

    resetFormData(){
       // console.log("[resetFormData]");
        
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.editingPerson = false;
    }

}

customElements.define('persona-form', PersonaForm);