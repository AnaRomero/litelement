import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';


class TestApi extends LitElement {
   
   //Propiedad para traer las películas
    static get properties(){
        return{
            movies:{type:Array}
        }
    }

    constructor(){
        super();
        //se inicializa con array vacío
        this.movies =[];
        this.getMovieData();
    }

    render(){
        return html`
            <!--<div>TestApi</div>-->
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
            
        `;
    }

    getMovieData(){
        console.log("[getMovieData] Obteniendo datos de las películas");

        //gestiona las peticiones asíncronas
        //si ponemos xhr se crea a nivel global. si ponemos let xhr se crea a nivel de bloque.
        let xhr = new XMLHttpRequest();

        //onload,es como un evento y se llama cuando se termina el envío de la petición, despues del send y open
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("[getMovieData] Petición completada correctamente");
                
                // En este punto, ya tenemos el json de la respuesta
                console.log("[getMovieData] "+ xhr.responseText);

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.movies = APIResponse.results;

            }
        };

        // abre el canal, prepara la petición , pero no se hace el envío todavía
        xhr.open("GET", "http://swapi.dev/api/films/");
        
        //IMPORTANTE: hace el envío. se envía aquí el body en caso de que el método tenga body. 
        //Get no tiene body, pero POST o PUT sí.
        xhr.send();
        console.log("[getMovieData] Fin de getMovieData");

    }
}

customElements.define('test-api', TestApi);